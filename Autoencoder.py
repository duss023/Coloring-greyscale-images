from keras.layers import Conv2D, UpSampling2D, InputLayer, Conv2DTranspose, Convolution2D, Input, MaxPooling2D, Reshape
from keras.layers import Activation, Dense, Dropout, Flatten
from keras.layers.normalization import BatchNormalization
from keras.models import Sequential, Model
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.callbacks import ModelCheckpoint, EarlyStopping
from skimage.color import rgb2lab, lab2rgb, rgb2gray, xyz2lab
from skimage.io import imsave
from skimage import transform
import numpy as np
import os
import random
import tensorflow as tf
import cv2

ROW = 256
COL = 256
CHANNELS = 3

TRAIN_DIR = 'Full-version/Train/'
TEST_DIR = 'Full-version/Test/'

train_images = [TRAIN_DIR+i for i in os.listdir(TRAIN_DIR)] 

def readImg(imgFile):
    colorImg = cv2.imread(imgFile, cv2.IMREAD_COLOR)
    colorImg = cv2.cvtColor(colorImg, cv2.COLOR_BGR2RGB)
    colorImg = cv2.resize(colorImg, (ROW, COL))/255.0
    greyImg = cv2.imread(imgFile, cv2.IMREAD_GRAYSCALE)
    greyImg = cv2.cvtColor(greyImg,cv2.COLOR_GRAY2RGB)
    greyImg = cv2.resize(greyImg, (ROW, COL))/255.0
    return greyImg,colorImg

def generateDate(imgFiles):
    count = len(imgFiles)
    dataX = np.ndarray((count, ROW, COL,CHANNELS), dtype=float)
    dataY = np.ndarray((count, ROW, COL,CHANNELS), dtype=float)
    for i, image_file in enumerate(imgFiles):
      gImg,cImg = readImg(image_file)
      dataX[i] = gImg
      dataY[i] = cImg  
    return dataX,dataY

import math
def chunked(iterable, n):
    chunksize = int(math.ceil(len(iterable) / n))
    return (iterable[i * chunksize:i * chunksize + chunksize]
            for i in range(n))

BATCHNUMBER= 10
chuckedTrainList = list(chunked(train_images,BATCHNUMBER))

# slice datasets for memory efficiency on Kaggle Kernels, delete if using full dataset
random.shuffle(train_images)

baseLevel = ROW//2//2
input_img = Input(shape=(ROW,COL,CHANNELS))
x = Convolution2D(256, 5, 5, activation='relu', border_mode='same')(input_img)
x = MaxPooling2D((2, 2), border_mode='same')(x)
x = Convolution2D(128, 5, 5, activation='relu', border_mode='same')(x)
x = MaxPooling2D((2, 2), border_mode='same')(x)
x = Flatten()(x)

encoded = Dense(100)(x)
one_d = Dense(baseLevel*baseLevel*128)(encoded)
fold = Reshape((baseLevel,baseLevel,128))(one_d)

x = UpSampling2D((2, 2))(fold)
x = Convolution2D(128, 5, 5, activation='relu', border_mode='same')(x)
x = UpSampling2D((2, 2))(x)
decoded = Convolution2D(3, 5, 5, activation='sigmoid', border_mode='same')(x)

autoencoder = Model(input_img, decoded)
autoencoder.compile(optimizer='adam', loss='binary_crossentropy')
checkpoint = ModelCheckpoint(filepath="path/weights.hdf5", verbose=1, save_best_only=True)
earlyStopping=EarlyStopping(monitor='val_loss', patience=0, verbose=0, mode='auto')

cbks = [checkpoint,earlyStopping]

digitalImg = dict()
epoch = 100

for ii in range(epoch):
    i = ii%len(chuckedTrainList)
    print("epoch =====>"+str(ii))
    imgfiles = chuckedTrainList[i]
    if str(i) in digitalImg:
        dataX = digitalImg[str(i)][0]
        dataY = digitalImg[str(i)][1]
    else:
        dataX,dataY = generateDate(imgfiles)
        digitalImg[str(i)] = (dataX,dataY)
        
    autoencoder.fit(dataX, dataY,
                epochs=1,
                batch_size=1,
                shuffle=True,
                verbose=2,
                #validation_split=0.3,
                callbacks=cbks)
