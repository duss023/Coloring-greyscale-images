from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.layers import Activation, Dense, Dropout, Flatten, InputLayer, Input, Reshape, merge, concatenate
from keras.layers.normalization import BatchNormalization
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from keras.models import Sequential, model_from_json, load_model, model_from_yaml, Model
from keras.layers.core import RepeatVector, Permute
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from skimage.color import rgb2lab, lab2rgb, rgb2gray, gray2rgb
from skimage.io import imsave
from skimage import transform
from skimage.transform import resize
import numpy as np
import os
import random
import tensorflow as tf
from keras.applications.inception_resnet_v2 import preprocess_input

# 画像読み込む
X = []
for filename in os.listdir('TrainX/'):
    imageX = img_to_array(load_img('TrainX/'+filename))
    imageX = transform.resize(imageX, (640, 800))
    X.append(imageX)
X = np.array(X, dtype=float)

# 訓練データ
split = int(1.0*len(X))
Xtrain = X[:split]
Xtrain = 1.0/255*Xtrain

Y = []
for filename in os.listdir('TrainY/'):
    imageY = img_to_array(load_img('TrainY/'+filename))
    imageY = transform.resize(imageY, (640, 800))
    Y.append(imageY)
Y = np.array(Y, dtype=float)
split = int(1.0*len(Y))
Ytrain = Y[:split]
Ytrain = 1.0/255*Ytrain

#Encoder
# encoder_input = Input(shape=(640, 800, 1,))
# encoder_output = Conv2D(32, (3,3), activation='relu', padding='same', strides=2)(encoder_input)
# encoder_output = Conv2D(64, (3,3), activation='relu', padding='same')(encoder_output)
# encoder_output = Conv2D(64, (3,3), activation='relu', padding='same', strides=2)(encoder_output)
# encoder_output = Conv2D(128, (3,3), activation='relu', padding='same')(encoder_output)
# encoder_output = Conv2D(128, (3,3), activation='relu', padding='same', strides=2)(encoder_output)
# encoder_output = Conv2D(256, (3,3), activation='relu', padding='same')(encoder_output)
# fusion_output = Conv2D(256, (1, 1), activation='relu', padding='same')(encoder_output) 
# decoder_output = Conv2D(128, (3,3), activation='relu', padding='same')(fusion_output)
# decoder_output = UpSampling2D((2, 2))(decoder_output)
# decoder_output = Conv2D(64, (3,3), activation='relu', padding='same')(decoder_output)
# decoder_output = UpSampling2D((2, 2))(decoder_output)
# decoder_output = Conv2D(32, (3,3), activation='relu', padding='same')(decoder_output)
# decoder_output = UpSampling2D((2, 2))(decoder_output)
# decoder_output = Conv2D(16, (3,3), activation='relu', padding='same')(decoder_output)
# decoder_output = Conv2D(8, (3,3), activation='relu', padding='same')(decoder_output)
# decoder_output = Conv2D(3, (3, 3), activation='tanh', padding='same')(decoder_output)
# model = Model(inputs=encoder_input, outputs=decoder_output)
# model.compile(optimizer='rmsprop', loss='mse')

# del model
model = load_model('output/toyota3_model.h5')
 
# Generate へ変換
batch_size = 1
def image_a_b_genX(X):
    
    lab_batch = rgb2lab(X)
    X_batch = lab_batch[:,:,:,0]
    return X_batch.reshape(X_batch.shape+(1,))

def image_a_b_genY(Y):
    lab_batch = rgb2lab(Y)
    Y_batch = lab_batch[:,:,:,:] / 128
    return Y_batch

iterx = image_a_b_genX(Xtrain)
itery = image_a_b_genY(Ytrain)

def getIter(iterx,itery):
    while 1:
        for index in range(iterx.shape[0]):
            print("index =====>"+str(index))
            yield(iterx[index].reshape((1,)+iterx[index].shape),itery[index].reshape((1,)+itery[index].shape))

# モデル続く訓練
checkpoint = ModelCheckpoint(filepath="output/toyota3_model.h5", verbose=1, monitor='loss', save_best_only=True, mode='min', period=1, save_weights_only=False)
cbks = [checkpoint]

for ii in range(5):
    print("epoch =====>"+str(ii))
    model.fit_generator(getIter(iterx,itery), callbacks=cbks, epochs=5, steps_per_epoch=1)
