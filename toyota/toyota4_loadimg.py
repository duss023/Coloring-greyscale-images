# load, split and scale the maps dataset ready for training
from os import listdir
from numpy import asarray
from numpy import vstack
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from numpy import savez_compressed

# load all images in a directory into memory
def load_images(Xpath,Ypath, size=(512,512)):
	src_list, tar_list = list(), list()
	listdirsX = os.listdir(Xpath)
	listdirsX.sort()
	for filename in listdirsX:
		pixels = load_img(Xpath + filename, target_size=size)
		pixels = img_to_array(pixels)
		src_list.append(pixels)
	listdirsY = os.listdir(Ypath)
	listdirsY.sort()
	for filename in listdirsY:
		pixels = load_img(Ypath + filename, target_size=size)
		pixels = img_to_array(pixels)
		tar_list.append(pixels)
	return [asarray(src_list), asarray(tar_list)]

# dataset path
Xpath = 'TrainX/'
Ypath = 'TrainY/'
# load dataset
[src_images, tar_images] = load_images(Xpath,Ypath)
print('Loaded: ', src_images.shape, tar_images.shape)
# save as compressed numpy array
filename = 'output/toyota4.npz'
savez_compressed(filename, src_images, tar_images)
print('Saved dataset: ', filename)