from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.layers import Activation, Dense, Dropout, Flatten, InputLayer, Input, Reshape, merge, concatenate
from keras.layers.normalization import BatchNormalization
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from keras.models import Sequential, model_from_json, load_model, model_from_yaml, Model
from keras.layers.core import RepeatVector, Permute
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from skimage.color import rgb2lab, lab2rgb, rgb2gray, gray2rgb
from skimage.io import imsave
from skimage import transform
from skimage.transform import resize
from keras.optimizers import SGD, Adam
import numpy as np
import os
import random
import tensorflow as tf
from keras.applications.inception_resnet_v2 import preprocess_input
from matplotlib import pyplot

def colorHand(rgb):
    # 緑との差
    green = abs(rgb[0]-79)
    green = green + abs(rgb[1]-255)
    green = green + abs(rgb[2]-130)
    # 赤との差
    red = abs(rgb[0]-255)
    red = red + abs(rgb[1]-0)
    red = red + abs(rgb[2]-0)
    #紫との差
    mura = abs(rgb[0]-198)
    mura = mura + abs(rgb[1]-118)
    mura = mura + abs(rgb[2]-255)
    #黄色との差
    yellow = abs(rgb[0]-244)
    yellow = yellow + abs(rgb[1]-244)
    yellow = yellow + abs(rgb[2]-45)

    if green < red and green < mura and green < yellow:
        return 0
    elif red < green and red < mura and red < yellow:
        return 1
    elif mura < green and mura < red and mura < yellow:
        return 2
    else:
        return 3

def transformY(Ytrain):
    transY = []
    for index in range(Ytrain.shape[0]):
        h,w,c = Ytrain[index].shape
        cur = np.zeros((h, w, 4))
        for i in range(h):
            for j in range(w):
                aras = []
                aras.append(colorHand(Ytrain[index][i][j]))
                aras = np.array(aras, dtype=int)
                cur[i][j] = (np.arange(4) == aras[:, None]).astype(int)
        transY.append(cur)
    return np.array(transY, dtype=int)

def toImg(npp):
    h,w,c = npp.shape
    cur = np.zeros((h, w, 3))
    for i in range(h):
        for j in range(w):
            result_max = np.argmax(npp[i][j], axis = 0)
            if result_max == 0:
                cur[i][j] =[79,255,130]
            elif result_max == 1:
                cur[i][j] =[255,0,0]
            elif result_max == 2:
                cur[i][j] =[198,118,255]
            else:
                cur[i][j] =[244,244,45]
    return cur

# 画像読み込む
Xtrain = []
listdirsX = os.listdir('TrainX/')
listdirsX.sort()
for filename in listdirsX:
    imageX = img_to_array(load_img('TrainX/'+filename))
    imageX = transform.resize(imageX, (688, 808))
    Xtrain.append(imageX)
Xtrain = np.array(Xtrain, dtype=float)
Xtrain = 1.0/255*Xtrain
Xtrain = rgb2lab(Xtrain)[:,:,:,0]
Xtrain = Xtrain.reshape(Xtrain.shape+(1,))

Ytrain = []
listdirsY = os.listdir('TrainY/')
listdirsY.sort()
for filename in listdirsY:
    imageY = img_to_array(load_img('TrainY/'+filename))
    imageY = transform.resize(imageY, (688, 808))
    Ytrain.append(imageY)
Ytrain = np.array(Ytrain, dtype=float)
Ytrain = transformY(Ytrain)


#Encoder
encoder_input = Input(shape=(688, 808, 1,))
encoder_output = Conv2D(32, (3,3), activation='relu', padding='same', strides=2)(encoder_input)
encoder_output = Conv2D(64, (3,3), activation='relu', padding='same')(encoder_output)
encoder_output = Conv2D(64, (3,3), activation='relu', padding='same', strides=2)(encoder_output)
encoder_output = Conv2D(128, (3,3), activation='relu', padding='same')(encoder_output)
encoder_output = Conv2D(128, (3,3), activation='relu', padding='same', strides=2)(encoder_output)
encoder_output = Conv2D(256, (3,3), activation='relu', padding='same')(encoder_output)
fusion_output = Conv2D(256, (1, 1), activation='relu', padding='same')(encoder_output) 
decoder_output = Conv2D(128, (3,3), activation='relu', padding='same')(fusion_output)
decoder_output = UpSampling2D((2, 2))(decoder_output)
decoder_output = Conv2D(64, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = UpSampling2D((2, 2))(decoder_output)
decoder_output = Conv2D(32, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = UpSampling2D((2, 2))(decoder_output)
decoder_output = Conv2D(16, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = Conv2D(8, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = Conv2D(4, (3, 3), activation='softmax', padding='same')(decoder_output)
model = Model(inputs=encoder_input, outputs=decoder_output)
adam = Adam(learning_rate=0.001, beta_1=0.999, beta_2=0.999, amsgrad=True)
model.compile(optimizer=adam,loss='binary_crossentropy',metrics=['accuracy'])

#model = load_model('output/toyota6_model_all.h5')
checkpoint = ModelCheckpoint(filepath="output/toyota6_model.h5", verbose=1, monitor='val_acc', save_best_only=True, mode='max', period=1, save_weights_only=False)
cbks = [checkpoint]
model.fit(Xtrain, Ytrain, epochs=200,batch_size=1,callbacks=cbks,validation_split=0.1,verbose=2)

del model
model = load_model('output/toyota6_model_all.h5')

color_me = []
for filename in os.listdir('Test/'):
    imageX = img_to_array(load_img('Test/'+filename))
    imageX = transform.resize(imageX, (688, 800))
    color_me.append(imageX)
color_me = np.array(color_me, dtype=float)
color_me = rgb2lab(1.0/255*color_me)[:,:,:,0]
color_me = color_me.reshape(color_me.shape+(1,))

# Test model
output = model.predict(color_me)

# Output colorizations
for i in range(len(output)):
    cur = np.zeros((688, 808, 3))
    cur = toImg(output[i])
    
    imsave("result/img_"+str(i)+".png", cur)