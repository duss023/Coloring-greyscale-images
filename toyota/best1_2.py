from keras.layers import Conv2D, UpSampling2D, InputLayer, Conv2DTranspose
from keras.layers import Activation, Dense, Dropout, Flatten
from keras.layers.normalization import BatchNormalization
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from skimage.color import rgb2lab, lab2rgb, rgb2gray, xyz2lab
from skimage.io import imsave
from skimage import transform
import numpy as np
import os
import random
import tensorflow as tf

imageX = img_to_array(load_img('toyota/TrainX/7-SEM-image.0000.tif'))
imageY = img_to_array(load_img('toyota/TrainY/7-SEM-image.0000.png'))

imageX = transform.resize(imageX, (640, 800))
imageY = transform.resize(imageY, (640, 800))

imageX = np.array(imageX, dtype=float)
imageY = np.array(imageY, dtype=float)

X = rgb2lab(1.0/255*imageX)[:,:,0]
Y = rgb2lab(1.0/255*imageY)

Y /= 128
X = X.reshape(1, 640, 800, 1)
Y = Y.reshape(1, 640, 800, 3)

model = Sequential()
model.add(InputLayer(input_shape=(None, None, 1)))
model.add(Conv2D(32, (3, 3), activation='relu', padding='same', strides=2))
model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same', strides=2))
model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(128, (3, 3), activation='relu', padding='same', strides=2))
model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
model.add(UpSampling2D((2, 2)))
model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
model.add(UpSampling2D((2, 2)))
model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
model.add(UpSampling2D((2, 2)))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(16, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(8, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(3, (3, 3), activation='tanh', padding='same'))

model.compile(optimizer='rmsprop',loss='mse')

model.fit(x=X, 
    y=Y,
    batch_size=1,
    epochs=500)

print(model.evaluate(X, Y, batch_size=1))
output = model.predict(X)
output *= 128
# Output colorizations
cur = np.zeros((640, 800, 3))
cur = output[0]

imsave("img_result_rgb.png", lab2rgb(cur))


