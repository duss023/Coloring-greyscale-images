from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.layers import Activation, Dense, Dropout, Flatten, InputLayer, Input, Reshape, merge, concatenate
from keras.layers.normalization import BatchNormalization
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from keras.models import Sequential, model_from_json, load_model, model_from_yaml, Model
from keras.layers.core import RepeatVector, Permute
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from skimage.color import rgb2lab, lab2rgb, rgb2gray, gray2rgb
from skimage.io import imsave
from skimage import transform
from skimage.transform import resize
from keras.optimizers import SGD, Adam
import numpy as np
import os
import random
import tensorflow as tf
from keras.applications.inception_resnet_v2 import preprocess_input
from matplotlib import pyplot

def toImg(npp):
    h,w,c = npp.shape
    cur = np.zeros((h, w, 3))
    for i in range(h):
        for j in range(w):
            result_max = np.argmax(npp[i][j], axis = 0)
            if result_max == 0:
                cur[i][j] =[79,255,130]
            elif result_max == 1:
                cur[i][j] =[255,0,0]
            elif result_max == 2:
                cur[i][j] =[198,118,255]
            else:
                cur[i][j] =[244,244,45]
    return cur

def resize8(i):
    if int(i/8) == i/8:
        return i
    else:
        return (int(i/8) + 1) * 8

model = load_model('output/toyota6_model.h5')

color_me = []
for filename in os.listdir('Test/'):
    color_me = []
    imageX = img_to_array(load_img('Test/'+filename))
    h,w,c = imageX.shape
    h8 = resize8(h)
    w8 = resize8(w)
    imageX = transform.resize(imageX, (h8, w8))
    color_me.append(imageX)
    color_me = np.array(color_me, dtype=float)
    color_me = rgb2lab(1.0/255*color_me)[:,:,:,0]
    color_me = color_me.reshape(color_me.shape+(1,))
    # Test model
    output = model.predict(color_me)
    # Output colorizations
    cur = np.zeros((h8, w8, 3))
    cur = toImg(output[0])
    imsave("result/"+filename+".png", cur)