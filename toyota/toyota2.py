from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.layers import Activation, Dense, Dropout, Flatten, InputLayer, Input, Reshape, merge, concatenate
from keras.layers.normalization import BatchNormalization
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from keras.models import Sequential, model_from_json, load_model, model_from_yaml, Model
from keras.layers.core import RepeatVector, Permute
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from skimage.color import rgb2lab, lab2rgb, rgb2gray, gray2rgb
from skimage.io import imsave
from skimage import transform
from skimage.transform import resize
import numpy as np
import os
import random
import tensorflow as tf
from keras.applications.inception_resnet_v2 import preprocess_input

# 画像読み込む
X = []
for filename in os.listdir('TrainX/'):
    imageX = img_to_array(load_img('TrainX/'+filename))
    imageX = transform.resize(imageX, (640, 800))
    X.append(imageX)
X = np.array(X, dtype=float)

# 訓練データ
split = int(0.9*len(X))
Xtrain = X[:split]
Xtest = X[split:]
Xtrain = 1.0/255*Xtrain
Xtest = 1.0/255*Xtest

Y = []
for filename in os.listdir('TrainY/'):
    imageY = img_to_array(load_img('TrainY/'+filename))
    imageY = transform.resize(imageY, (640, 800))
    Y.append(imageY)
Y = np.array(Y, dtype=float)
split = int(0.9*len(Y))
Ytrain = Y[:split]
Ytest = Y[split:]
Ytrain = 1.0/255*Ytrain
Ytest = 1.0/255*Ytest


#Load weights
# inception = InceptionResNetV2(weights='imagenet', include_top=True)
# inception.graph = tf.get_default_graph()


#Encoder
embed_input = Input(shape=(1000,))
encoder_input = Input(shape=(640, 800, 1,))
encoder_output = Conv2D(32, (3,3), activation='relu', padding='same', strides=2)(encoder_input)
encoder_output = Conv2D(64, (3,3), activation='relu', padding='same')(encoder_output)
encoder_output = Conv2D(64, (3,3), activation='relu', padding='same', strides=2)(encoder_output)
encoder_output = Conv2D(128, (3,3), activation='relu', padding='same')(encoder_output)
encoder_output = Conv2D(128, (3,3), activation='relu', padding='same', strides=2)(encoder_output)
encoder_output = Conv2D(256, (3,3), activation='relu', padding='same')(encoder_output)

#Fusion
# fusion_output = RepeatVector(80 * 100)(embed_input) 
# fusion_output = Reshape(([80, 100, 1000]))(fusion_output)
# fusion_output = concatenate([encoder_output, fusion_output], axis=3) 
fusion_output = Conv2D(256, (1, 1), activation='relu', padding='same')(encoder_output) 

#Decoder
decoder_output = Conv2D(128, (3,3), activation='relu', padding='same')(fusion_output)
decoder_output = UpSampling2D((2, 2))(decoder_output)
decoder_output = Conv2D(64, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = UpSampling2D((2, 2))(decoder_output)
decoder_output = Conv2D(32, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = UpSampling2D((2, 2))(decoder_output)
decoder_output = Conv2D(16, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = Conv2D(8, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = Conv2D(3, (3, 3), activation='tanh', padding='same')(decoder_output)

model = Model(inputs=encoder_input, outputs=decoder_output)

def create_inception_embedding(grayscaled_rgb):
    grayscaled_rgb_resized = []
    for i in grayscaled_rgb:
        i = resize(i, (299, 299, 3), mode='constant')
        grayscaled_rgb_resized.append(i)
    grayscaled_rgb_resized = np.array(grayscaled_rgb_resized)
    grayscaled_rgb_resized = preprocess_input(grayscaled_rgb_resized)
    with inception.graph.as_default():
        embed = inception.predict(grayscaled_rgb_resized)
    return embed
#モデル読み込む
# model = Sequential()
# model.add(InputLayer(input_shape=(640, 800, 1)))
# model.add(Conv2D(32, (3, 3), activation='relu', padding='same', strides=2))
# model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
# model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
# model.add(Conv2D(64, (3, 3), activation='relu', padding='same', strides=2))
# model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
# model.add(Conv2D(128, (3, 3), activation='relu', padding='same', strides=2))
# model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
# model.add(UpSampling2D((2, 2)))
# model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
# model.add(UpSampling2D((2, 2)))
# model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
# model.add(UpSampling2D((2, 2)))
# model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
# model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
# model.add(Conv2D(16, (3, 3), activation='relu', padding='same'))
# model.add(Conv2D(8, (3, 3), activation='relu', padding='same'))
# model.add(Conv2D(3, (3, 3), activation='tanh', padding='same'))

# yaml_file = open('output/toyota2_model.yaml', 'r') 
# loaded_model_yaml = yaml_file.read() 
# yaml_file.close()
# model = model_from_yaml(loaded_model_yaml) # load weights into new model loaded_model.load_weights("model.h5") print("Loaded model from disk")
# model.load_weights('output/toyota2_weight.h5')

# with open(r'output/toyota2_model.json', 'r') as file:
#     model_json = file.read()
 
# #加载模型
# model = model_from_json(model_json)


#model.load_weights('output/toyota2_weight.h5')
model.compile(optimizer='rmsprop', loss='mse')


#model = load_model('output/toyota2_model.h5')

# Generate へ変換
batch_size = 1
def image_a_b_genX(X):
    
    lab_batch = rgb2lab(X)
    X_batch = lab_batch[:,:,:,0]
    return X_batch.reshape(X_batch.shape+(1,))

def image_a_b_genY(Y):
    lab_batch = rgb2lab(Y)
    Y_batch = lab_batch[:,:,:,:] / 128
    return Y_batch

iterx = image_a_b_genX(Xtrain)
itery = image_a_b_genY(Ytrain)

def getIter(iterx,itery):
    while 1:
        for index in range(iterx.shape[0]):
            grayscaled_rgb = gray2rgb(iterx[index])
            yield(iterx[index].reshape((1,)+iterx[index].shape),itery[index].reshape((1,)+itery[index].shape))

# モデル続く訓練
checkpoint = ModelCheckpoint(filepath="output/toyota2_weight.h5", verbose=1, monitor='loss', save_best_only=True, mode='min', period=1, save_weights_only=True)
cbks = [checkpoint]

for ii in range(1):
    print("epoch =====>"+str(ii))
    model.fit_generator(getIter(iterx,itery), callbacks=cbks, epochs=1, steps_per_epoch=1)

model.save('output/toyota2_model.h5')
del model
model = load_model('output/toyota2_model.h5')
# model_yaml = model.to_yaml()
# with open("output/toyota2_model.yaml", "w") as yaml_file:
#     yaml_file.write(model_yaml)

# model_json = model.to_json()
# with open(r'output/toyota2_model.json', 'w') as file:
#     file.write(model_json)



# # テスト
# Xtest = rgb2lab(1.0/255*X[split:])[:,:,:,0]
# Xtest = Xtest.reshape(Xtest.shape+(1,))
# Ytest = rgb2lab(1.0/255*X[split:])[:,:,:,:]
# Ytest = Ytest / 128
# print(model.evaluate(Xtest, Ytest, batch_size=batch_size))

# color_me = []
# for filename in os.listdir('Test/'):
#     image = img_to_array(load_img('Test/'+filename))
#     image = transform.resize(image, (640, 800))
#     color_me.append(image)
# color_me = np.array(color_me, dtype=float)
# color_me = rgb2lab(1.0/255*color_me)[:,:,:,0]
# color_me = color_me.reshape(color_me.shape+(1,))

# # Test model
# output = model.predict(color_me)
# output = output * 128

# # Output colorizations
# for i in range(len(output)):
#     cur = np.zeros((640, 800, 3))
#     cur = output[i]
#     imsave("result/img_"+str(i)+".png", lab2rgb(cur))