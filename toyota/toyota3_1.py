from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.layers import Activation, Dense, Dropout, Flatten, InputLayer, Input, Reshape, merge, concatenate
from keras.layers.normalization import BatchNormalization
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
from keras.models import Sequential, model_from_json, load_model, model_from_yaml, Model
from keras.layers.core import RepeatVector, Permute
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from skimage.color import rgb2lab, lab2rgb, rgb2gray, gray2rgb
from skimage.io import imsave
from skimage import transform
from skimage.transform import resize
import numpy as np
import os
import random
import tensorflow as tf
from keras.applications.inception_resnet_v2 import preprocess_input
from matplotlib import pyplot

def printX(listX):
    for index in range(listX.shape[0]):
        output = np.zeros((640, 800, 3))
        output[:,:,:] = Xtrain[index]
        pyplot.imshow(output)
        pyplot.axis('off')
        pyplot.savefig('result/X'+ str(index) +'.png')
        pyplot.close()

def printY(listY):
    for index in range(listY.shape[0]):
        pyplot.imshow(lab2rgb(listY[index] * 128))
        pyplot.axis('off')
        pyplot.savefig('result/Y'+ str(index) +'.png')
        pyplot.close()

# 画像読み込む
Xtrain = []
listdirsX = os.listdir('TrainX/')
listdirsX.sort()
for filename in listdirsX:
    imageX = img_to_array(load_img('TrainX/'+filename))
    imageX = transform.resize(imageX, (640, 800))
    Xtrain.append(imageX)
Xtrain = np.array(Xtrain, dtype=float)
Xtrain = 1.0/255*Xtrain
#printX(Xtrain)
Xtrain = rgb2lab(Xtrain)[:,:,:,0]
Xtrain = Xtrain.reshape(Xtrain.shape+(1,))

Ytrain = []
listdirsY = os.listdir('TrainY/')
listdirsY.sort()
for filename in listdirsY:
    imageY = img_to_array(load_img('TrainY/'+filename))
    imageY = transform.resize(imageY, (640, 800))
    Ytrain.append(imageY)
Ytrain = np.array(Ytrain, dtype=float)
Ytrain = 1.0/255*Ytrain
Ytrain = rgb2lab(Ytrain)[:,:,:,:]/128

#printY(Ytrain)

#Encoder
encoder_input = Input(shape=(640, 800, 1,))
encoder_output = Conv2D(32, (3,3), activation='relu', padding='same', strides=2)(encoder_input)
encoder_output = Conv2D(64, (3,3), activation='relu', padding='same')(encoder_output)
encoder_output = Conv2D(64, (3,3), activation='relu', padding='same', strides=2)(encoder_output)
encoder_output = Conv2D(128, (3,3), activation='relu', padding='same')(encoder_output)
encoder_output = Conv2D(128, (3,3), activation='relu', padding='same', strides=2)(encoder_output)
encoder_output = Conv2D(256, (3,3), activation='relu', padding='same')(encoder_output)
fusion_output = Conv2D(256, (1, 1), activation='relu', padding='same')(encoder_output) 
decoder_output = Conv2D(128, (3,3), activation='relu', padding='same')(fusion_output)
decoder_output = UpSampling2D((2, 2))(decoder_output)
decoder_output = Conv2D(64, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = UpSampling2D((2, 2))(decoder_output)
decoder_output = Conv2D(32, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = UpSampling2D((2, 2))(decoder_output)
decoder_output = Conv2D(16, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = Conv2D(8, (3,3), activation='relu', padding='same')(decoder_output)
decoder_output = Conv2D(3, (3, 3), activation='tanh', padding='same')(decoder_output)
model = Model(inputs=encoder_input, outputs=decoder_output)
model.compile(optimizer='rmsprop', loss='mse')

# del model
#model = load_model('output/toyota3_model.h5')

# モデル続く訓練
checkpoint = ModelCheckpoint(filepath="output/toyota3_model_60.h5", verbose=1, monitor='loss', save_best_only=True, mode='min', period=1, save_weights_only=False)
cbks = [checkpoint]

for ii in range(500):
    print("epoch =====>"+str(ii))
    #model.fit_generator(getIter(iterx,itery), callbacks=cbks, epochs=1, steps_per_epoch=1)
    model.fit(Xtrain, Ytrain)
model.save("output/toyota3_model_60.h5")

