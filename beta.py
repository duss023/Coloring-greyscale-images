from keras.layers import Conv2D, Conv2DTranspose, UpSampling2D
from keras.layers import Activation, Dense, Dropout, Flatten, InputLayer
from keras.layers.normalization import BatchNormalization
from keras.callbacks import TensorBoard
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from skimage.color import rgb2lab, lab2rgb, rgb2gray
from skimage.io import imsave
from skimage import transform
import numpy as np
import os
import random
import tensorflow as tf

# Get images
X = []
for filename in os.listdir('toyota/TrainX/'):
    imageX = img_to_array(load_img('toyota/TrainX/'+filename))
    imageX = transform.resize(imageX, (640, 800))
    X.append(imageX)
X = np.array(X, dtype=float)

# Set up train and test data
split = int(0.80*len(X))
Xtrain = X[:split]
Xtrain = 1.0/255*Xtrain


Y = []
for filename in os.listdir('toyota/TrainY/'):
    imageY = img_to_array(load_img('toyota/TrainY/'+filename))
    imageY = transform.resize(imageY, (640, 800))
    Y.append(imageY)
Y = np.array(Y, dtype=float)

# Set up train and test data
split = int(0.80*len(Y))
Ytrain = Y[:split]
Ytrain = 1.0/255*Ytrain

model = Sequential()
model.add(InputLayer(input_shape=(640, 800, 1)))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same', strides=2))
model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(128, (3, 3), activation='relu', padding='same', strides=2))
model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(256, (3, 3), activation='relu', padding='same', strides=2))
model.add(Conv2D(512, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
model.add(UpSampling2D((2, 2)))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
model.add(UpSampling2D((2, 2)))
model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
model.add(UpSampling2D((2, 2)))
model.add(Conv2D(2, (3, 3), activation='tanh', padding='same'))
model.compile(optimizer='rmsprop', loss='mse')

# Image transformer
datagen = ImageDataGenerator(
        shear_range=0.2,
        zoom_range=0.2,
        rotation_range=20,
        horizontal_flip=True)

# Generate training data
batch_size = 5
def image_a_b_genX(batch_size):
    iterx = []
    for batch in datagen.flow(Xtrain, batch_size=batch_size):
        lab_batch = rgb2lab(batch)
        X_batch = lab_batch[:,:,:,0]
        iterx.append(X_batch.reshape(X_batch.shape+(1,)))
        #yield (X_batch.reshape(X_batch.shape+(1,)))
    return iterx

def image_a_b_genY(batch_size):
    itery = []
    for batch in datagen.flow(Ytrain, batch_size=batch_size):
        lab_batch = rgb2lab(batch)
        Y_batch = lab_batch[:,:,:,1:] / 128
        itery.append(Y_batch)
        #yield (Y_batch)
    return itery

iterx = image_a_b_genX(batch_size)
itery = image_a_b_genY(batch_size)

def getIter(iterx,itery):
    for i in iterx.len():
        yield(iterx[i],itery[i])

# Train model 
tensorboard = TensorBoard(log_dir="toyota/output/first_run")

model.fit_generator(getIter(iterx,itery), callbacks=[tensorboard], epochs=500, steps_per_epoch=1)

# Save model
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights("model.h5")

# Test images
Xtest = rgb2lab(1.0/255*X[split:])[:,:,:,0]
Xtest = Xtest.reshape(Xtest.shape+(1,))
Ytest = rgb2lab(1.0/255*X[split:])[:,:,:,1:]
Ytest = Ytest / 128
print(model.evaluate(Xtest, Ytest, batch_size=batch_size))

color_me = []
for filename in os.listdir('toyota/Test/'):
    color_me.append(img_to_array(load_img('toyota/Test/'+filename)))
color_me = np.array(color_me, dtype=float)
color_me = rgb2lab(1.0/255*color_me)[:,:,:,0]
color_me = color_me.reshape(color_me.shape+(1,))

# Test model
output = model.predict(color_me)
output = output * 128

# Output colorizations
for i in range(len(output)):
    cur = np.zeros((640, 800, 3))
    cur[:,:,0] = color_me[i][:,:,0]
    cur[:,:,1:] = output[i]
    imsave("toyota/result/img_"+str(i)+".png", lab2rgb(cur))